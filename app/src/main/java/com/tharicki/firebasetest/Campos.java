package com.tharicki.firebasetest;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Campos {

    private String campo1;
    private String campo2;

    public Campos() {
    }

    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public String getCampo2() {
        return campo2;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("campo1", campo1);
        result.put("campo2", campo2);

        return result;
    }

}
